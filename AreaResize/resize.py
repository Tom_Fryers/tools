#!/usr/bin/env python3
"""
Resize an image by gamma-correct area-averaging.

Either or both x and y may be given. If only one is, the other will be
calculated to maintain aspect ratio.
"""
import argparse
import itertools
import math
from fractions import Fraction
from functools import cache

from PIL import Image

Image.MAX_IMAGE_PIXELS = 1_000_000_000
BAR_LENGTH = 78


def weighted_average(values, weights=None):
    """Calculate a weighted average of some values."""
    if not values:
        raise ValueError("Need at least one value to average")
    if weights is None:
        return math.fsum(values) / len(values)
    if len(weights) != len(values):
        raise ValueError("Length of values must match length of weights")
    return math.fsum(v * w for v, w in zip(values, weights)) / math.fsum(weights)


@cache
def sRGB(colour):
    """Apply gamma-correction to a colour."""
    new_colour = []
    for u in colour:
        u /= 255
        if u <= 0.0031308:
            new_colour.append(12.92 * u)
        else:
            new_colour.append(1.055 * u ** (1 / 2.4) - 0.055)
    return tuple(new_colour)


@cache
def unsRGB(colour):
    """Undo gamma-correction to get back linear values."""
    new_colour = []
    for u in colour:
        if u <= 0.04045:
            new_colour.append(u / 12.92)
        else:
            new_colour.append(((u + 0.055) / 1.055) ** 2.4)
    return tuple(u * 255 for u in new_colour)


def colour_average(colours, weights):
    if len(colours[0]) == 4:
        alpha = round(weighted_average([c[3] for c in colours], weights))
        colours = [c[:3] for c in colours]
    else:
        alpha = None
    colours = [unsRGB(c) for c in colours]
    colour = sRGB(weighted_average([c[i] for c in colours], weights) for i in range(3))
    colour = [round(x) for x in colour]
    if alpha is None:
        return tuple(colour)
    colour.append(alpha)
    return tuple(colour)


def complete_dimensions(old, new):
    """Calculate the other image dimension if only one is given."""
    if new[0] is None:
        return (round(old[0] * new[1] / old[1]), new[1])
    if new[1] is None:
        return (new[0], round(old[1] * new[0] / old[0]))
    return new


def get_coordinates(left, right, bottom, top):
    """Return an iterator of all 2D grid coordinates within the given range."""
    return itertools.product(
        range(int(left), math.ceil(right)), range(int(top), math.ceil(bottom))
    )


def range_intersection(v1_min, v1_max, v2_min, v2_max):
    """Return the size of the intersection of both ranges."""
    return max(0, min(v1_max, v2_max) - max(v1_min, v2_min))


def get_new_pixel_value(x, y, source_pixels, scale):
    """Calculate pixel value the pixels in the original image."""
    left = x * scale[0]
    right = (x + 1) * scale[0]
    top = y * scale[1]
    bottom = (y + 1) * scale[1]

    cols = [
        source_pixels[subx, suby]
        for subx, suby in get_coordinates(left, right, bottom, top)
    ]
    if len(set(cols)) == 1:
        return cols[0]

    weights = [
        range_intersection(subx, subx + 1, left, right)
        * range_intersection(suby, suby + 1, top, bottom)
        for subx, suby in get_coordinates(left, right, bottom, top)
    ]
    return colour_average(cols, weights)


def get_scale_offset(old_size, new_size, aspect_ratio_mode):
    if old_size[0] * new_size[1] != old_size[1] * new_size[0]:
        if aspect_ratio_mode == "error":
            raise ValueError("dimensions not exact")
        if aspect_ratio_mode == "stretch":
            return (
                Fraction(old_size[0], new_size[0]),
                Fraction(old_size[1], new_size[1]),
            ), (0, 0)
        elif aspect_ratio_mode == "crop":
            scale = min(
                Fraction(old_size[0], new_size[0]), Fraction(old_size[1], new_size[1])
            )
    else:
        scale = Fraction(old_size[0], new_size[0])
    return (scale, scale), [
        (Fraction(old_size[i], scale) - new_size[i]) / 2 for i in range(2)
    ]


def resize(image, new_dims, aspect_ratio_mode, progress=False):
    if progress:
        print("[" + "-" * BAR_LENGTH + "]\n[", end="")
        segments_done = 0
    px = image.load()
    new_dims = complete_dimensions(image.size, new_dims)
    scale, offset = get_scale_offset(image.size, new_dims, aspect_ratio_mode)
    newim = Image.new(image.mode, new_dims)
    newpx = newim.load()
    for new_x in range(new_dims[0]):
        for new_y in range(new_dims[1]):
            newpx[new_x, new_y] = get_new_pixel_value(
                new_x + offset[0], new_y + offset[1], px, scale
            )
        if progress:
            while round(new_x / new_dims[0] * BAR_LENGTH) > segments_done:
                print("#", end="", flush=True)
                segments_done += 1
    if progress:
        print("]")
    return newim


def main():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("-p", "--progress", action="store_true", help="show progress")
    parser.add_argument("-x", "--width", type=int, help="width of output image")
    parser.add_argument("-y", "--height", type=int, help="height of output image")
    parser.add_argument(
        "-a",
        "--aspect-ratio",
        choices=("error", "crop", "stretch"),
        default="error",
        help="what to do when the input and output aspect ratios are different",
    )
    parser.add_argument(
        "input", metavar="in", type=argparse.FileType("rb"), help="input image file"
    )
    parser.add_argument(
        "output", metavar="out", type=argparse.FileType("wb"), help="output image file"
    )
    args = parser.parse_args()
    input_image = Image.open(args.input)
    if args.width is None and args.height is None:
        parser.error("must supply width or height")
    if (
        args.width is not None
        and args.width <= 0
        or args.height is not None
        and args.height <= 0
    ):
        parser.error("width and height must be positive")
    try:
        resize(
            input_image,
            (args.width, args.height),
            args.aspect_ratio,
            progress=args.progress,
        ).save(args.output)
    except ValueError as e:
        parser.error(str(e))


if __name__ == "__main__":
    main()
