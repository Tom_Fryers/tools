#!/usr/bin/env python3
"""Change a race time by scaling the time of every point."""

import argparse
import re
from datetime import datetime, timedelta


def parse_timedelta(time: str) -> timedelta:
    return timedelta(
        seconds=sum(60**i * int(n) for i, n in enumerate(time.split(":")[::-1]))
    )


def retime(gpx: str, time: timedelta) -> str:
    print(gpx)
    times = re.findall(r"<time>(.*)Z</time>", gpx)
    start_time = datetime.fromisoformat(times[0])
    finish_time = datetime.fromisoformat(times[-1])
    total_time = finish_time - start_time
    ratio = time / total_time
    for point_time in times:
        new_point_time = (
            (datetime.fromisoformat(point_time) - start_time) * ratio + start_time
        ).isoformat()
        gpx = gpx.replace(point_time, new_point_time)
    return gpx


def main():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument(
        "time",
        type=str,
        help="new time",
    )
    parser.add_argument(
        "-i",
        "--in-file",
        metavar="FILENAME",
        type=argparse.FileType("r"),
        help="filename to read from",
    )
    parser.add_argument(
        "-o",
        "--out-file",
        metavar="FILENAME",
        type=argparse.FileType("w"),
        help="filename to write to",
    )
    args = parser.parse_args()
    with args.in_file as in_f, args.out_file as out_f:
        out_f.write(retime(gpx=in_f.read(), time=parse_timedelta(args.time)))


if __name__ == "__main__":
    main()
