#!/usr/bin/env python3
"""Transfer music metadata between tags and TSV files."""
import argparse
import itertools
import unicodedata
from collections.abc import Iterable, Iterator, Sequence
from dataclasses import dataclass
from pathlib import Path

from mutagen.easyid3 import EasyID3
from mutagen.flac import FLAC

EasyID3.RegisterTextKey("grouping", "TIT1")
EasyID3.RegisterTextKey("label", "TPUB")


@dataclass
class MusicFile:
    path: Path

    def __post_init__(self) -> None:
        if self.path.suffix == ".flac":
            self.filetype = "flac"
            self.obj: FLAC | EasyID3 = FLAC(self.path)
        elif self.path.suffix == ".mp3":
            self.filetype = "mp3"
            self.obj = EasyID3(self.path)
        else:
            raise ValueError(f"Unrecognised music type for {self.path}")

    def __getitem__(self, tag: str) -> str:
        values = self.obj[tag]
        if len(values) > 1:
            raise ValueError(f"tag {tag} for {self} has multiple values")
        return values[0]

    def __setitem__(self, tag: str, value: str) -> None:
        self.obj[tag] = value

    def __delitem__(self, tag) -> None:
        del self.obj[tag]

    @property
    def tags(self) -> dict[str, str]:
        return {t: self[t] for t in self.obj}

    def save(self) -> None:
        self.obj.save()


def load_tsv(fileobj: Iterable[str]) -> list[list[str]]:
    return [r.rstrip("\n").split("\t") for r in fileobj if r]


def save_tsv(fileobj, table: Iterable[Iterable[str]]) -> None:
    fileobj.writelines("\t".join(r) + "\n" for r in table)


def list_files(directory: str) -> list[Path]:
    return [x for x in sorted(Path(directory).glob("**/*")) if not x.is_dir()]


def music_files(directory: str) -> Iterator[tuple[str, Path]]:
    for i, filename in enumerate(list_files(directory)):
        if filename.suffix in {".flac", ".mp3"}:
            yield (hex(i), filename)
        elif filename.suffix in {".jpg", ".png"}:
            continue
        else:
            raise ValueError(f"Unrecognised file type for {filename}")


def load_from_dir(directory: str) -> list[dict[str, str]]:
    files = []
    for i, filename in music_files(directory):
        musicfile = MusicFile(filename)
        files.append(musicfile.tags)
        files[-1]["id"] = i
    return files


def write_to_dir(directory: str, files: dict[str, dict[str, str]]) -> None:
    done = set()
    for i, filename in music_files(directory):
        try:
            table_tags = files[i]
        except KeyError:
            print(f"DELETING {filename}")
            filename.unlink()
            continue
        musicfile = MusicFile(filename)
        file_tags = musicfile.tags
        changes = []
        for tag in file_tags.keys() | table_tags.keys():
            if tag not in file_tags:
                changes.append(f"added tag: {tag} = {table_tags[tag]}")
                musicfile[tag] = table_tags[tag]
            elif tag not in table_tags:
                changes.append(f"removed tag: {tag} = {file_tags[tag]}")
                del musicfile[tag]
            elif file_tags[tag] != table_tags[tag]:
                changes.append(
                    f"changed tag: {tag} = {table_tags[tag]} (<- {file_tags[tag]})"
                )
                musicfile[tag] = table_tags[tag]
        if changes:
            change_string = "\n".join("    " + i for i in changes)
            print(f"Changes for {filename}:\n{change_string}")
            # if input():
            musicfile.save()
        done.add(i)
    bad_ids = set(files) - done
    if bad_ids:
        print(f"Bad IDs: {bad_ids}")


BAD_CHARS = "/\\:|?<>*.[]="


def safe_file_name(string: str) -> str:
    return "".join(
        c
        for c in unicodedata.normalize(
            "NFKD", string.replace("\N{EN DASH}", "-").replace('"', "'")
        )
        .encode("ascii", "ignore")
        .decode("ascii")
        if c not in BAD_CHARS
    )


def new_name(musicfile: MusicFile, suffix: str) -> str:
    main_part = safe_file_name(f"{musicfile['grouping']} - {musicfile['title']}")
    directory = safe_file_name(musicfile["album"])
    max_len = 255 - len(suffix)
    if len(main_part.encode()) > max_len or len(directory) > 150:
        raise ValueError("Filename too long")
    return f"{directory}/{main_part}{suffix}"


def move_files(directory: str) -> None:
    for _, filename in music_files(directory):
        musicfile = MusicFile(filename)
        new_path = Path(directory) / new_name(musicfile, filename.suffix)
        if filename == new_path:
            continue
        print(f"Moving\n    {filename}\n -> {new_path}")
        if new_path.exists():
            raise FileExistsError(f"{new_path} exists")
        new_path.parent.mkdir(exist_ok=True)
        filename.replace(new_path)


def to_string(cell: str | Sequence[object]) -> str:
    if isinstance(cell, str):
        return cell
    if len(cell) == 1:
        return str(cell[0])
    raise ValueError(f"Cannot encode {cell!r}")


def main() -> None:
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("music-dir", type=Path, help="directory for music")
    parser.add_argument(
        "-i",
        "--tsv-input",
        metavar="FILE",
        type=argparse.FileType(mode="r"),
        help="TSV file to take metadata from",
    )
    parser.add_argument(
        "-o",
        "--tsv-output",
        metavar="FILE",
        type=argparse.FileType(mode="w"),
        help="TSV file to write metadata to",
    )
    parser.add_argument(
        "-r",
        "--rename",
        action="store_true",
        help="just rename files based on their metadata",
    )
    args = parser.parse_args()
    music_dir = getattr(args, "music-dir")
    if args.rename:
        move_files(music_dir)
        return

    if args.tsv_input is None and args.tsv_output is None:
        parser.error("one of the arguments -i -o is required")
    if args.tsv_output is not None:
        files = load_from_dir(music_dir)
        tags = set(itertools.chain.from_iterable(files))

        table = [list(tags)] + [
            [to_string(m[t]) if t in m else "" for t in tags] for m in files
        ]
        save_tsv(args.tsv_output, table)

    if args.tsv_input is not None:
        head, *table = load_tsv(args.tsv_input)
        file_metadata = {}
        for row in table:
            file_id = row[head.index("id")]
            file_metadata[file_id] = {
                h: r for h, r in zip(head, row) if h != "id" and r
            }
        write_to_dir(music_dir, file_metadata)
        move_files(music_dir)


if __name__ == "__main__":
    main()
