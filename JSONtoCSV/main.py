#!/usr/bin/env python3
"""
Convert a file from JSON to CSV format.

The key argument is given with a separating / between the column name
and the location specifier. The location specifier is a colon-seperated
list of regular expressions (one regex for each layer of the JSON file
up to the key level).  Leave this blank to search all items or a list.
After the key has been found as a JSON dictionary key, the rest of the
columns are searched for in a similar way on the value.
"""
import json
import re


def generate_CSV_text(headings, data):
    """Convert a list of lists to CSV format."""
    heading_row = ",".join(headings) + "\n"
    return heading_row + "\n".join(
        ",".join(str(column[i]) for column in data) for i in range(len(data[0]))
    )


def search_JSON_tree(key_layers, data):
    """Recursively find a matching key within data."""
    values = {}
    if isinstance(data, dict):
        for key, value in data.items():
            if key_layers[0]:
                m = re.match(key_layers[0], key)
                if not m:
                    continue
                m = m.string
            else:
                m = key
            if len(key_layers) == 1:
                values[m] = value
            else:
                values.update(search_JSON_tree(key_layers[1:], value))

    elif isinstance(data, list):
        for value in data:
            values += search_JSON_tree(key_layers[1:], value)

    return values


def main():
    import argparse
    import sys

    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument(
        "input", type=argparse.FileType("r"), help="Filename of JSON to convert"
    )
    parser.add_argument(
        "output", type=argparse.FileType("w"), help="Filename of CSV to produce"
    )
    parser.add_argument(
        "key_column", metavar="key", help="Location of names of each row"
    )
    parser.add_argument(
        "columns", nargs="*", help="Locations of each data field to extract"
    )

    args = parser.parse_args()
    try:
        data = json.loads(args.input.read())
    except json.decoder.JSONDecodeError:
        sys.exit("Error: invalid JSON file")

    key_layers = args.key_column.split("/")[1].split(":")
    row_key_layers = [r.split("/")[1].split(":") for r in args.columns]

    key_values = search_JSON_tree(key_layers, data)

    columns = [list(key_values.keys())]
    for row in row_key_layers:
        columns.append(
            [list(search_JSON_tree(row, v).values())[0] for v in key_values.values()]
        )

    headings = [args.key_column.split("/")[0]] + [r.split("/")[0] for r in args.columns]
    args.output.write(generate_CSV_text(headings, columns))


if __name__ == "__main__":
    main()
